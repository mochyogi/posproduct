<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

?>
<div id="daftarPegawai-index">
	<div class="row" style="margin-bottom: 10px">
		<div class="col-md-12">
	        <?php $form= ActiveForm::begin()?>
				<?= Html::input('text','pencarian','', $options=['class'=>'form-control', 'id'=>'pencarianPegawai']) ?>
			<?php ActiveForm::end(); ?>	
		</div>
	</div>
	<div id="daftarPegawaiContent">
		<?=
		    ListView::widget([
		        'dataProvider' => $dataProvider,
		        'options' => [
		            'tag' => 'div',
		            'class' => 'row',
		            // 'id' => 'list-wrapper',
		        ],
		        'itemView' => function ($model, $key, $index, $widget) {
		            return $this->render('listPegawai', ['model' => $model]);
		        },
		        'itemOptions' => [
		            'tag' => false
		        ],
		        'summary' => '',
		        'layout' => '{items} {pager}',

		        'pager' => [
		            'firstPageLabel' => 'First',
		            'lastPageLabel' => 'Last',
		            'maxButtonCount' => 4,
		            'options' => [
		                'class' => 'pagination col-md-12'
		            ]
		        ],
		    ]);
		?>
	</div>
</div>