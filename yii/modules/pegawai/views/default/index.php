<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\CustomHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\widgets\ListView;

$this->title = Yii::t('app', 'Pegawai');
$this->params['breadcrumbs'][] = $this->title;
$menu = 11;
$detailMenu = CustomHelper::getDetailMenu($menu);

?>
<div class="pegawai-index">
	<div class="sb-page-header pb-10 sb-page-header-dark bg-gradient-primary-to-secondary">
	    <div class="container-fluid">
	        <div class="sb-page-header-content py-5">
	            <h1 class="sb-page-header-title">
	                <div class="sb-page-header-icon"><i data-feather="activity"></i></div>
	                <span>Pegawai</span>
	            </h1>
	            <div class="sb-page-header-subtitle">Semua data yang berhubungan dengan pegawai</div>
	        </div>
	    </div>
	</div>
	<div class="container-fluid mt-n10">
		<div class="card">
		    <div class="card-header">
		        <ul class="nav nav-pills card-header-pills" id="cardPill" role="tablist">
		        	<?php foreach ($detailMenu as $key => $value):?>
		            <li class="nav-item"><a class="nav-link <?=($value['order'] == 1)? 'active': '';?>" onclick="clicks('<?=$value['code']?>')" id="<?=$value['code']?>" href="#<?=$value['code']?>Pill" data-toggle="tab" role="tab" aria-controls="<?= $value['code']?>" aria-selected="true"><?= $value['deskripsi']?></a></li>
		        	<?php endforeach;?>
		        </ul>
		    </div>
		    <div class="card-body">
		        <div class="tab-content" id="cardPillContent">
		        	<?php foreach ($detailMenu as $key => $value):?>
		            <div class="tab-pane fade <?=($value['order'] == 1)? 'show active': '';?>" id="<?= $value['code']?>Pill" role="tabpanel" aria-labelledby="<?= $value['code']?>">
			      		<div id="<?= $value['code']?>-content"></div>
		            </div>
		        	<?php endforeach;?>
		        </div>
		    </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	clicks('daftar-pegawai');
	function clicks(id){
		setPartial(id);
	}

	function setPartial(id){
	    var csrfToken = $('meta[name="csrf-token"]').attr("content");
	    $.ajax({
	      url: '<?= Url::to(['get-']) ?>'+id,
	      cache: false,
	      type: 'post',
	      data: {_csrf : csrfToken, id:id},
	      dataType: 'JSON',
			// beforeSend: function(){
		 //      	$('.tab-content').empty();
			// },
	        success: function(response) {
	          $('#'+id+'-content').html(response);
	        }
	    });
  	};
</script>