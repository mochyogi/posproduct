<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pegawai-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-8">
        <?= $form->field($modelBiodata, 'nama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelBiodata, 'tgl_lahir')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-md-4">
        <?= Yii::$app->controller->renderAjax('/../../admin/views/general/_formSingleFile', ['form' => $form, 'modelFile'=> $modelFile]);?>        
    </div>

    <?= Yii::$app->controller->renderAjax('/../../admin/views/general/_formAlamat', ['form' => $form, 'modelAlamat'=> $modelAlamat]);?>
    
    <?= Yii::$app->controller->renderAjax('/../../admin/views/general/_formUserJabatan', ['form' => $form, 'modelUserJabatan'=> $modelUserJabatan]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
