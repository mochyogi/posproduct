<?php
// _list_item.php
use app\components\WebHelper;
use yii\helpers\Url;
use app\components\CustomHelper;
use yii\helpers\Html;
// $listgroup = CustomHelper::listGroup($model['id']);
$style = 'style="background-color: #f1f1f1"';
// die(var_dump($model));
?>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
    <div class="card sb-card-header-actions mx-auto">
         <div class="card-header">
            <?= $model->nama;?>
            <div>
                <button class="btn btn-pink sb-btn-icon mr-2">
                    <i data-feather="heart"></i>
                </button>
                <button class="btn btn-teal sb-btn-icon mr-2">
                    <i data-feather="bookmark"></i>
                </button>
                <button class="btn btn-blue sb-btn-icon">
                    <i data-feather="share"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4"><img class="img-fluid" src="<?= CustomHelper::getFile($model['file']['id'])?>" alt="..."></div>
                <div class="col-md-8">
                    <div class="card-body">
<!--                         <h5 class="card-title">Card Image (Left)</h5> -->
                        <p class="card-text"><?= $model['deskripsiJabatan']['deskripsi']?></p>
                        <p class="card-text"><?= $model['usaha']['nama_usaha']?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function reload(){
        $.ajax({
            url: '<?= Url::to(['/admin/m-event/index']) ?>',
            type: 'get',
            success: function(tes) {
                $('#viewEvent').html(tes);
            }
        });
    }

    function hapusEvent(id){
        var csrfToken = $('meta[name="csrf-token"]').attr("content");
        swal({
            title: "Apakah anda yakin?",
            icon: "warning",
            buttons: ['tidak', 'yakin'],
            dangerMode: true,
        })
        .then((willDelete) => {
            $('#viewEvent').loading('start');
            if (willDelete) {
                $.ajax({
                    url: '<?= Url::to(['/admin/m-event/delete-ajax']) ?>?id='+id,
                    cache: false,
                    type: 'post',
                    data: {_csrf : csrfToken},
                    dataType: 'JSON',
                    success: function(response) {
                        if(response.status == 1) {
                            reload();
                            $('#viewEvent').loading('stop');
                            // swal(response.message, {
                            //   icon: "success",
                            // });
                        }else{
                            $('#viewEvent').loading('stop');
                            // swal(response.message);
                        }
                    }
                });
            } else {
                $('#viewEvent').loading('stop');
            }
        });

    }
</script>