<?php 

namespace app\modules\pegawai\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MMaster;
use app\models\MMasterSearch;
use app\models\PegawaiSearch;
use app\models\MBiodata;
use app\models\MAlamat;
use app\models\TFile;
use app\models\TUserJabatan;
use app\components\CustomHelper;

class DefaultController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

	public function actionIndex(){
        $searchModel = new MMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
	}

	public function actionGetDaftarPegawai(){
        $param  = Yii::$app->request->queryParams;
        $searchModel = new PegawaiSearch();
        $dataProvider = $searchModel->search($param);
		$partial = $this->renderPartial('daftarPegawai', ['dataProvider' => $dataProvider]);
		return json_encode($partial);
	}

	public function actionGetTambahPegawai(){
        $modelBiodata = new MBiodata();
        $modelAlamat = new MAlamat();
        $modelFile = new TFile();
        $modelUserJabatan = new TUserJabatan();

        if ($modelBiodata->load(Yii::$app->request->post())) {
            die(var_dump(Yii::$app->request->post()));
            var_dump($modelBiodata);
            var_dump($modelAlamat);
            var_dump($modelFile);
            var_dump($modelUserJabatan);
            die();
        	$modelBiodata->save();
            return $this->redirect(['index']);
        }
        $partial = $this->renderAjax('create/create', ['modelBiodata' => $modelBiodata, 'modelAlamat' => $modelAlamat, 'modelFile' => $modelFile, 'modelUserJabatan' => $modelUserJabatan]); 
        return json_encode($partial);
	}
}