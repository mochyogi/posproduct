<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\CustomHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\widgets\ListView;

$this->title = Yii::t('app', 'Konfigurasi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barang-index">
	<div class="sb-page-header pb-10 sb-page-header-dark bg-gradient-primary-to-secondary">
	    <div class="container-fluid">
	        <div class="sb-page-header-content py-5">
	            <h1 class="sb-page-header-title">
	                <div class="sb-page-header-icon"><i data-feather="activity"></i></div>
	                <span>Konfigurasi</span>
	            </h1>
	            <div class="sb-page-header-subtitle">Semua data yang berhubungan dengan konfigurasi perusahaan</div>
	        </div>
	    </div>
	</div>
	<div class="container-fluid mt-n10">
		<div class="card">
		    <div class="card-header">
		        <ul class="nav nav-pills card-header-pills" id="cardPill" role="tablist">
		        	<?php foreach ($detailMenu as $key => $value):?>
		            <li class="nav-item"><a class="nav-link <?=($value['order'] == 1)? 'active': '';?>" onclick="clicks('<?=$value['code']?>')" id="<?=$value['code']?>" href="#<?=$value['code']?>Pill" data-toggle="tab" role="tab" aria-controls="<?= $value['code']?>" aria-selected="true"><?= $value['deskripsi']?></a></li>
		        	<?php endforeach;?>
		        </ul>
		    </div>
		    <div class="card-body">
		        <div class="tab-content" id="cardPillContent">
		        	<?php foreach ($detailMenu as $key => $value):?>
		            <div class="tab-pane fade <?=($value['order'] == 1)? 'show active': '';?>" id="<?= $value['code']?>Pill" role="tabpanel" aria-labelledby="<?= $value['code']?>">
		            	<center class='loadingClass' style='display: none;'>
		            		<div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status">
							  <span class="sr-only">Loading...</span>
							</div>
							<br/>
							<span>Prepare Page</span>
		            	</center>
			      		<div id="<?= $value['code']?>-content"></div>
		            </div>
		        	<?php endforeach;?>
		        </div>
		    </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	clicks('daftar-bisnis');
	function clicks(id){
		setPartial(id);
	}

	function setPartial(id){
	    var csrfToken = $('meta[name="csrf-token"]').attr("content");
	    $.ajax({
	      url: '<?= Url::to(['get-']) ?>'+id,
	      cache: false,
	      type: 'post',
	      data: {_csrf : csrfToken, id:id},
	      dataType: 'JSON',
	      beforeSend:function(){
	      	$('#'+id+'-content').html('');
	      	$('.loadingClass').show();
	      },
          success: function(response) {
          	$('.loadingClass').hide();
          	$('#'+id+'-content').html(response);
          },error:function(e,msg){
          	$('.loadingClass').hide();
          	$('#'+id+'-content').html(e.status + ' ' +e.statusText);
          }
	    });
  	};
</script>