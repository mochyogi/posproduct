<?php 
$no=1; 
?>
<div class="sb-page-header pb-10 sb-page-header-dark bg-gradient-primary-to-secondary">
    <div class="container-fluid">
        <div class="sb-page-header-content py-5">
            <h1 class="sb-page-header-title">
                <div class="sb-page-header-icon"><i data-feather="activity"></i></div>
                <span>Kategori</span>
            </h1>
            <div class="sb-page-header-subtitle">Kategori Item</div>
        </div>
        <div class="card mb-4">
			<div class="card-header">Data Kategori</div>
			<div class="card-body">
				<div class="btn btn-primary float-right">Input Kategori</div>
				<div class="sb-datatable table-responsive">
	                <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
	                    <thead>
	                    	<tr>
	                    		<th>No</th>
	                    		<th>Kategori</th>
	                    		<th>Actions</th>
	                    	</tr>
	                	</thead>
	                	<tbody>
	                		<?php foreach($model as $value):?>
	                		<tr>
	                			<td><?= $no ?></td>
	                			<td><?= $value['deskripsi']?></td>
	                            <td>
	                                <button class="btn sb-btn-datatable sb-btn-icon sb-btn-transparent-dark mr-2"><i data-feather="more-vertical"></i></button><button class="btn sb-btn-datatable sb-btn-icon sb-btn-transparent-dark"><i data-feather="trash-2"></i></button>
	                            </td>
	                		</tr>
	                    	<?php $no++; ?>
	                    	<?php endforeach;?>
	                	</tbody>
	                </table>
				</div>
			</div>
		</div>
    </div>
</div>

			
		