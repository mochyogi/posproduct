<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pegawai-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-8">
        <?= $form->field($modelBiodata, 'nama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($modelBiodata, 'tgl_lahir')->textInput(['maxlength' => true]) ?>
    </div>
    
    <div class="col-md-4">
        <?= Yii::$app->controller->renderAjax('/../../admin/views/general/_formSingleFile', ['modelFile'=> $modelFile]);?>        
    </div>

    <?= Yii::$app->controller->renderAjax('/../../admin/views/general/_formAlamat', ['modelAlamat'=> $modelAlamat]);?>
    
    <?= Yii::$app->controller->renderAjax('/../../admin/views/general/_formUserJabatan', ['modelUserJabatan'=> $modelUserJabatan]);?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
