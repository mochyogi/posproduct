<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MMaster */
// $this->title = Yii::t('app', 'Tambah Pegawai');
// $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'M Masters'), 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mmaster-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'modelBiodata' => $modelBiodata,
        'modelAlamat' => $modelAlamat,
        'modelFile' => $modelFile,
        'modelUserJabatan' => $modelUserJabatan,
    ]) ?>

</div>
