<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MMaster;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\MMaster */
/* @var $form yii\widgets\ActiveForm */
$role=MMaster::find()->where(['type'=> 'role', 'status' => 1])->all();
$listData=ArrayHelper::map($role,'id','deskripsi');
?>

<div class="user-jabatan-form">


    <?= $form->field($modelUserJabatan, 'id_user')->textInput(['maxlength' => true]) ?>

    <?=  $form->field($modelUserJabatan, 'id_role')->dropDownList(
        $listData,
        ['prompt'=>'Pilih Jabatan ...']
        )->label('Jabatan');
    ?>

    <?= $form->field($modelUserJabatan, 'id_usaha')->textInput() ?>


</div>
