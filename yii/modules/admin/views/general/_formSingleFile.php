<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="single-file-form">


    <?= $form->field($modelFile, 'file_name')->textInput(['maxlength' => true]) ?>


</div>
