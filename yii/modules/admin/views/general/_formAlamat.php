<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alamat-form">


    <?= $form->field($modelAlamat, 'alamat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelAlamat, 'id_negara')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelAlamat, 'id_provinsi')->textInput() ?>

    <?= $form->field($modelAlamat, 'id_kota')->textInput() ?>

    <?= $form->field($modelAlamat, 'id_kecamatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($modelAlamat, 'kode_pos')->textInput() ?>


</div>
