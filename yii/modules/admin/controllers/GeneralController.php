<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MMaster;
use app\components\CustomHelper;

class GeneralController extends Controller
{
    public function actionCreateAlamat()
    {
        $model = new MAlamat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('_createAlamat', [
            'model' => $model,
        ]);
    }
}