<?php
namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MMaster;
use app\models\MMasterSearch;
use app\models\MUsahaSearch;
use app\models\MUsaha;
use app\models\PerusahaanSearch;
use app\components\CustomHelper;

class DefaultController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

	public function actionIndex(){
        $searchModel = new MMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $menu = 12;
        $detailMenu = MMaster::find()->where(['parent' => $menu])->all();
        return $this->render('page/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'detailMenu'=>$detailMenu,
        ]);
	}

	public function actionGetDaftarBisnis(){
		$searchModel = new PerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$partial = $this->renderAjax('page/daftarBisnis', ['dataProvider' => $dataProvider]);
		return json_encode($partial);	
	}

	public function actionGetBisnisUtama(){
		$kategoriBarang = MMaster::find()->where(['type' => 'menu'])->all();
		$model = new MUsaha();

		if ($model->load(Yii::$app->request->post())) {
			$model->created_at = date('Y-m-d H:i:s');
			$model->created_by = Yii::$app->user->identity->username;
			$model->save();
            return $this->redirect(['index', 'id' => 'bisnisUtama']);
        }

		$partial = $this->renderAjax('page/bisnisUtama', ['model' => $model]);
		return json_encode($partial);	
	}

	public function actionGetCabangBisnis(){
		$searchModel = new PerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$partial = $this->renderAjax('page/cabangBisnis', ['dataProvider' => $dataProvider]);
		return json_encode($partial);
	}

	public function actionGetGudang(){
		$searchModel = new PerusahaanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$partial = $this->renderAjax('page/gudang', ['dataProvider' => $dataProvider]);
		return json_encode($partial);	
	}

	public function actionKategori(){
		$model = MMaster::find()->where(['type' => 'kategori'])->all();
		return $this->render('kategori/index', ['model' => $model]);
	}

	public function actionSatuan(){
		$model = MMaster::find()->where(['type' => 'satuan'])->all();
		return $this->render('satuan/index', ['model'=> $model]);
	}

	public function actionTipeHarga(){
		$model = MMaster::find()->where(['type' => 'tipeHarga'])->all();
		return $this->render('tipeharga/index', ['model' => $model]);
	}

	public function actionItem(){
		$model = MMaster::find()->where(['type' => 'kategori'])->all();
		return $this->render('item/index');
	}

	public function actionHarga(){
		$model = MMaster::find()->where(['type' => 'kategori'])->all();
		return $this->render('harga/index');
	}

	public function actionOutlet(){
		return $this->render('outlet/index');
	}

	// public function action
}