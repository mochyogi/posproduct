<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;

?>
<div id="daftarBarang-index">
	<div class="row">
		<?php $form = ActiveForm::begin(['options' => [
                'style' => 'width:100%'
             ]
         ]); ?>
			<?= Html::input('text','pencarian','', $options=['class'=>'form-control', 'id'=>'pencarianBarang']) ?>
		<?php ActiveForm::end(); ?>	
	</div>
	<div id="barangContent">
		<?=
		    ListView::widget([
		        'dataProvider' => $dataProvider,
		        'options' => [
		            'tag' => 'div',
		            'class' => 'row',
		            // 'id' => 'list-wrapper',
		        ],
		        'itemView' => function ($model, $key, $index, $widget) {
		            return $this->render('listBarang', ['model' => $model]);
		        },
		        'itemOptions' => [
		            'tag' => false
		        ],
		        'summary' => '',
		        'layout' => '{items} {pager}',

		        'pager' => [
		            'firstPageLabel' => 'First',
		            'lastPageLabel' => 'Last',
		            'maxButtonCount' => 4,
		            'options' => [
		                'class' => 'pagination col-md-12'
		            ]
		        ],
		    ]);
		?>
	</div>
</div>