<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\CustomHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\widgets\ListView;

$this->title = Yii::t('app', 'Barang');
$this->params['breadcrumbs'][] = $this->title;
$menu = 10;
$detailMenu = CustomHelper::getDetailMenu($menu);

$this->registerCss('
    .form-tambah {
        margin-top: calc(-1.5em + 2px);
        margin-bottom: 1.35rem;
        margin-right: -2rem;
        margin-left: -2rem;
        padding-left: 2rem;
        padding-right: 2rem;
    }
');
?>
<div class="barang-index">
	<div class="sb-page-header pb-10 sb-page-header-dark bg-gradient-primary-to-secondary">
	    <div class="container-fluid">
	        <div class="sb-page-header-content py-5">
	            <h1 class="sb-page-header-title">
	                <div class="sb-page-header-icon"><i data-feather="activity"></i></div>
	                <span>Barang</span>
	            </h1>
	            <div class="sb-page-header-subtitle">Semua data yang berhubungan dengan barang</div>
	        </div>
	    </div>
	</div>
	<div class="container-fluid mt-n10">
		<div class="card">
		    <div class="card-header">
		        <ul class="nav nav-pills card-header-pills" id="cardPill" role="tablist">
		        	<?php foreach ($detailMenu as $key => $value):?>
		            <li class="nav-item"><a class="nav-link <?=($value['order'] == 1)? 'active': '';?>" onclick="clicks('<?=$value['code']?>')" id="<?=$value['code']?>" href="#<?=$value['code']?>Pill" data-toggle="tab" role="tab" aria-controls="<?= $value['code']?>" aria-selected="true"><?= $value['deskripsi']?></a></li>
		        	<?php endforeach;?>
		        </ul>
		    </div>
		    <div class="card-body">
		        <div class="tab-content" id="cardPillContent">
		        	<?php foreach ($detailMenu as $key => $value):?>
		            <div class="tab-pane fade <?=($value['order'] == 1)? 'show active': '';?>" id="<?= $value['code']?>Pill" role="tabpanel" aria-labelledby="<?= $value['code']?>">
		      		<div id="<?= $value['code']?>-content"></div>
		            </div>
		        	<?php endforeach;?>
		        </div>
		    </div>
		</div>
	</div>
</div>
<script type="text/javascript">
	clicks('daftar-barang');
	function clicks(id){
		setPartial(id);
	}

	function setPartial(id){
	    var csrfToken = $('meta[name="csrf-token"]').attr("content");
	    $.ajax({
	      url: '<?= Url::to(['get-']) ?>'+id,
	      cache: false,
	      type: 'post',
	      data: {_csrf : csrfToken, id:id},
	      dataType: 'JSON',
			// beforeSend: function(){
		 //      	$('.tab-content').empty();
			// },
	        success: function(response) {
	          $('#'+id+'-content').html(response);
	        }
	    });
  	};
	  
	function saveItem(url, data) {

		Swal.queue([{
			title: 'Tambah data?',
			confirmButtonText: 'Tambah',
			showLoaderOnConfirm: true,
			preConfirm: () => {
				return postData(url, data)
				.then((data) => {
					setPartial(data.pagename);
				});
			}
		}]);
	}

	async function postData(url = '', data = {}) {
		var form_data = new FormData();

		for ( var key in data ) {
			form_data.append(data[key].name, data[key].value);
		}
		const response = await fetch(url, {
			method: 'POST',
			mode: 'cors',
			cache: 'no-cache',
			credentials: 'same-origin',
			redirect: 'follow',
			referrerPolicy: 'no-referrer',
			body: form_data
		});
		return await response.json();
	}
</script>