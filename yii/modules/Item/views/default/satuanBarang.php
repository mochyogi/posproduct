<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\editable\Editable;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use app\models\MMaster;
$model = new MMaster;
$modelClass = substr(strrchr(get_class($model), "\\"), 1);
$satuan = MMaster::find()->where(['type' => 'satuan'])->all();
$listData=ArrayHelper::map($satuan,'id','deskripsi');
/* @var $this yii\web\View */
/* @var $searchModel app\models\MMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = Yii::t('app', 'M Masters');
// $this->params['breadcrumbs'][] = $this->title;
$this->registerJs('
    $(\'.form-item\').submit(function(e) {
        e.preventDefault();

        var element     = $(this),
            url         = element.attr(\'action\'),
            data        = element.serializeArray();
            
        data.push({
            name: \'MMaster[pagename]\',
            value: \''.Yii::$app->request->post()['id'].'\'
        });

        saveItem(url, data);
    });
');
?>
<div class="mmaster-index">

    <?php Pjax::begin([
            'id'=>'pjaxSatuan', 
            'enablePushState'=>false,
            'timeout'=>100000
        ]); 
    ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="d-block form-tambah bg-light border-top border-bottom py-4">
        <div class="text-right">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-add-satuan-barang" aria-expanded="false" aria-controls="form-add-satuan-barang">
                <i class="fas fa-plus"></i> &nbsp; 
                Tambah Satuan
            </button>
        </div>

        <div class="collapse" id="form-add-satuan-barang">
            <div class="card card-body shadow-sm border border-primary mt-3">
                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['/item/default/save']),
                    'options' => [
                        'class' => 'form-item'
                    ]
                ]); ?>

                    <?= Html::hiddenInput("{$modelClass}[type]", 'satuanBarang') ?>

                    <?= Html::hiddenInput("{$modelClass}[status]", 1) ?>

                    <?= Html::hiddenInput("{$modelClass}[parent]", 0) ?>

                    <?= $form->field($model, 'deskripsi') ?>

                    <?= $form->field($model, 'parent')->dropDownList(
                        $listData,
                        ['prompt'=>'Select...']
                        );
                    ?>

                    <div class="form-group mb-0">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'id' => 'gridSatuan',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table table-sm'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Satuan',
                'format' => 'raw',
                'value' => function($data) {
                    return Editable::widget([
                        'model'=>$data, 
                        'attribute' => 'deskripsi',
                        'size' => 'md',
                        'format' => 'button',
                        'pjaxContainerId' => 'pjaxSatuan',
                        'editableValueOptions'=>['class'=>''],
                        'format' => Editable::FORMAT_LINK,
                        'formOptions' => [
                            'action' => Url::toRoute(['/item/default/save', 'dataid' => $data->id]),
                            'method' => 'post'
                        ],
                    ]);
                }
            ],
            [
                'label' => 'Induk Satuan',
                'attribute' => 'deskripsiParent',
                'value' => function($data){
                    // return ($data->deskripsi) ? $data->deskripsi : '';
                    return ($data->parentDeskripsi) ? $data->parentDeskripsi->deskripsi : '';
                }
            ],
            //'status',
            // 'parent',
            //'code',
            //'order',
            //'created_by',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'widget:100px, align:center;'],
                'header' => 'Actions',
                'template' => '{hapus} &nbsp ',
                'buttons' => [
                    'hapus' => function($url, $model, $key) {
                        return Html::a(' hapus', ['delete', 'id'=>$model->id], [
                            'class' => 'btn btn-primary btn-xs',
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
