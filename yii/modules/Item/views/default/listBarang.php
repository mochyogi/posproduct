<?php 

use yii\widgets\ListView;
use yii\helpers\Html;

?>
<div class="card">
    <div class="row no-gutters">
        <div class="col-md-4"><img class="img-fluid" src="path/to/image" alt="..."></div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">Card Image (Left)</h5>
                <p class="card-text">...</p>
            </div>
        </div>
    </div>
</div>