<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\editable\Editable;
use app\models\MMaster;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = Yii::t('app', 'M Masters');
// $this->params['breadcrumbs'][] = $this->title;

$model = new MMaster;
$modelClass = substr(strrchr(get_class($model), "\\"), 1);

$this->registerJs('
    $(\'.form-item\').submit(function(e) {
        e.preventDefault();

        var element     = $(this),
            url         = element.attr(\'action\'),
            data        = element.serializeArray();
            
        data.push({
            name: \'MMaster[pagename]\',
            value: \''.Yii::$app->request->post()['id'].'\'
        });

        saveItem(url, data);
    });
');

?>
<div class="mmaster-index">

    <?php Pjax::begin([
            'id'=>'pjaxTipeHarga', 
            'enablePushState'=>false,
            'timeout'=>100000
        ]);
    ?>

    <div class="d-block form-tambah bg-light border-top border-bottom py-4">
        <div class="text-right">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-add-tipe-harga" aria-expanded="false" aria-controls="form-add-tipe-harga">
                <i class="fas fa-plus"></i> &nbsp; 
                Tambah Tipe Harga
            </button>
        </div>

        <div class="collapse" id="form-add-tipe-harga">
            <div class="card card-body shadow-sm border border-primary mt-3">
                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['/item/default/save']),
                    'options' => [
                        'class' => 'form-item'
                    ]
                ]); ?>

                    <?= Html::hiddenInput("{$modelClass}[type]", 'tipeHarga') ?>

                    <?= Html::hiddenInput("{$modelClass}[status]", 1) ?>

                    <?= Html::hiddenInput("{$modelClass}[parent]", 0) ?>

                    <?= $form->field($model, 'deskripsi') ?>

                    <?= $form->field($model, 'code') ?>

                    <div class="form-group mb-0">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'id' => 'gridTipeHarga',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table table-sm'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Deskripsi',
                'format' => 'raw',
                'value' => function($data) {
                    return Editable::widget([
                        'model'=>$data, 
                        'attribute' => 'deskripsi',
                        'size' => 'md',
                        'format' => 'button',
                        'pjaxContainerId' => 'pjaxKategori',
                        'editableValueOptions'=>['class'=>''],
                        'format' => Editable::FORMAT_LINK,
                        'formOptions' => [
                            'action' => Url::toRoute(['/item/default/save', 'dataid' => $data->id]),
                            'method' => 'post'
                        ],
                    ]);
                }
            ],
            [
                'label' => 'Tipe Harga',
                'format' => 'raw',
                'value' => function($data) {
                    return Editable::widget([
                        'model'=>$data, 
                        'attribute' => 'code',
                        'size' => 'md',
                        'format' => 'button',
                        'pjaxContainerId' => 'pjaxKategori',
                        'editableValueOptions'=>['class'=>''],
                        'format' => Editable::FORMAT_LINK,
                        'formOptions' => [
                            'action' => Url::toRoute(['/item/default/save', 'dataid' => $data->id]),
                            'method' => 'post'
                        ],
                    ]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'widget:100px, align:center;'],
                'header' => 'Actions',
                'template' => '{hapus} &nbsp ',
                'buttons' => [
                    'hapus' => function($url, $model, $key) {
                        return Html::a(' hapus', ['delete', 'id'=>$model->id], [
                            'class' => 'btn btn-primary btn-xs',
                        ]);
                    },
                ]
             ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
