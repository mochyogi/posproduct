<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\MMaster;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\editable\Editable;
/* @var $this yii\web\View */
/* @var $searchModel app\models\MMasterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$model = new MMaster;
$modelClass = substr(strrchr(get_class($model), "\\"), 1);
// $this->title = Yii::t('app', 'M Masters');
// $this->params['breadcrumbs'][] = $this->title;

$this->registerJs('
	$(\'.form-item\').submit(function(e) {
		e.preventDefault();

        var element 	= $(this),
            url         = element.attr(\'action\'),
            data		= element.serializeArray();
            
        data.push({
            name: \'MMaster[pagename]\',
            value: \''.Yii::$app->request->post()['id'].'\'
        });

        saveItem(url, data);
	});
');

?>
<div class="mmaster-index">

    <?php Pjax::begin([
            'id'=>'pjaxKategori', 
            'enablePushState'=>false,
            'timeout'=>100000
        ]);
    ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="d-block form-tambah bg-light border-top border-bottom py-4">
        <div class="text-right">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-add-kategori-barang" aria-expanded="false" aria-controls="form-add-kategori-barang">
                <i class="fas fa-plus"></i> &nbsp; 
                Tambah Kategori
            </button>
        </div>

        <div class="collapse" id="form-add-kategori-barang">
            <div class="card card-body shadow-sm border border-primary mt-3">
                <?php $form = ActiveForm::begin([
                    'action' => Url::toRoute(['/item/default/save']),
                    'options' => [
                        'class' => 'form-item'
                    ]
                ]); ?>

                    <?= Html::hiddenInput("{$modelClass}[type]", 'kategoriBarang') ?>

                    <?= Html::hiddenInput("{$modelClass}[status]", 1) ?>

                    <?= Html::hiddenInput("{$modelClass}[parent]", 0) ?>

                    <?= $form->field($model, 'deskripsi') ?>

                    <div class="form-group mb-0">
                        <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <?= GridView::widget([
        'id' => 'gridKategori',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => [
            'class' => 'table table-sm'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Deskripsi',
                'format' => 'raw',
                'value' => function($data) {
                    return Editable::widget([
                        'model'=>$data, 
                        'attribute' => 'deskripsi',
                        'size' => 'md',
                        'format' => 'button',
                        'pjaxContainerId' => 'pjaxKategori',
                        'editableValueOptions'=>['class'=>''],
                        'format' => Editable::FORMAT_LINK,
                        'formOptions' => [
                            'action' => Url::toRoute(['/item/default/save', 'dataid' => $data->id]),
                            'method' => 'post'
                        ],
                    ]);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'widget:100px, align:center;'],
                'header' => 'Actions',
                'template' => '{hapus} &nbsp ',
                'buttons' => [
                    'hapus' => function($url, $model, $key) {
                        return Html::a(' hapus', ['delete', 'id'=>$model->id], [
                            'class' => 'btn btn-primary btn-xs',
                        ]);
                    },
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
