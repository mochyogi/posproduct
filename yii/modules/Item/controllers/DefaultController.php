<?php 

namespace app\modules\item\controllers;

use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MMaster;
use app\models\MMasterSearch;
use app\models\ItemSearch;
use app\models\MItem;
use app\components\CustomHelper;

class DefaultController extends Controller
{
	public function actionIndex(){
        $searchModel = new MMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
	}

	public function actionSave($dataid = null)
	{
		$post = Yii::$app->request->post();
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		if(true) {	
			if(! empty($post['MMaster'])) {

				if(! empty($dataid)) {
					$model = MMaster::find()->where(['id' => $dataid])->one();
					$model->updated_at = date('Y-m-d H:i:s');
				} else {
					$model = new MMaster();

					$post['MMaster']['created_at'] = date('Y-m-d H:i:s');
					$post['MMaster']['updated_at'] = date('Y-m-d H:i:s');
					if(empty($post['MMaster']['parent'])){
						$post['MMaster']['parent'] = 0;	
					}
				}
				

				if ($model->load($post) && $model->save()) {
					$array = [
						'status' => 'success',
						'data' => $model
					];

					if(! empty($post['MMaster']['pagename'])) {
						$array['pagename'] = $post['MMaster']['pagename'];
					}

					return $array;
				}
			} 
		}
	}

	public function actionGetDaftarBarang(){
		$searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$partial = $this->renderAjax('daftarBarang', ['dataProvider' => $dataProvider]);
		return json_encode($partial);
	}

	public function actionGetKategoriBarang(){
        $searchModel = new MMasterSearch();
        $param = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->searchKategori($param);
		$partial = $this->renderAjax('kategoriBarang', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
		return json_encode($partial);
	}

	public function actionGetSatuanBarang(){
        $searchModel = new MMasterSearch();
        $param = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->searchSatuan($param);
		$partial = $this->renderAjax('satuanBarang', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
		return json_encode($partial);
	}

	public function actionGetTipeHargaBarang(){
        $searchModel = new MMasterSearch();
        $param = Yii::$app->request->queryParams;

        $dataProvider = $searchModel->searchTipeHarga($param);
		$partial = $this->renderAjax('tipeHargaBarang', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
		return json_encode($partial);
	}

	public function actionGetTambahBarang(){
		$kategoriBarang = MMaster::find()->where(['type' => 'menu'])->all();
		$model = new MItem();

		if ($model->load(Yii::$app->request->post())) {
			$model->created_at = date('Y-m-d H:i:s');
			$model->created_by = Yii::$app->user->identity->username;
			$model->save();
            return $this->redirect(['index', 'id' => 'tambahBarang']);
        }

		$partial = $this->renderAjax('tambahBarang', ['model' => $model]);
		return $partial;
	}

	public function actionTambahHargaBarang(){
		$kategoriBarang = MMaster::find()->where(['type' => 'menu'])->all();
		$partial = $this->renderAjax('kategoriBarang', ['kategoriBarang' => $kategoriBarang]);
		return $partial;
	}

	public function actionDelete($id){
		$model = MMaster::find()->where(['id' => $id])->one();
		if(!empty($model)){
			$model->delete();
			$data['info'] = 'Data berhasil dihapus';
		}else{
			$data['info'] = 'Data tidak ditemukan';
		}
		return json_encode($data);
	}
}