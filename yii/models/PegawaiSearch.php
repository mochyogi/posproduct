<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MBiodata;
use app\models\MAlamat;
use app\models\TFile;

/**
 * PegawaiSearch represents the model behind the search form of `app\models\MBiodata`.
 */
class PegawaiSearch extends MBiodata
{
    /**
     * {@inheritdoc}
     */

    public $alamat;
    public $file;
    public $jabatan;
    public function rules()
    {
        return [
            [['id_user', 'id_role', 'jenis_kelamin'], 'integer'],
            [['tgl_lahir', 'created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 255],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MBiodata::find();
        $query->innerJoinWith('alamat', true);
        $query->innerJoinWith('file', true);
        $query->innerJoinWith('deskripsiJabatan', true);
        $query->innerJoinWith('usaha', true);
        // $query->leftJoin('t_file', 'm_biodata.id = t_file.id_relasi AND t_file.id_jenis=48');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['attributes' => ['nama', 'jenis_kelamin']]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nama' => $this->nama,
            'jenis_kelamin' => $this->jenis_kelamin,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
