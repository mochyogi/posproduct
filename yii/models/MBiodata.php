<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_biodata".
 *
 * @property int $id
 * @property int|null $id_user
 * @property int|null $id_role
 * @property string|null $nama
 * @property string|null $tgl_lahir
 * @property int|null $jenis_kelamin 0 perempuan, 1 lelaki
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 */
class MBiodata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_biodata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_role', 'jenis_kelamin'], 'integer'],
            [['tgl_lahir', 'created_at', 'updated_at'], 'safe'],
            [['nama'], 'string', 'max' => 255],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_role' => Yii::t('app', 'Id Role'),
            'nama' => Yii::t('app', 'Nama'),
            'tgl_lahir' => Yii::t('app', 'Tgl Lahir'),
            'jenis_kelamin' => Yii::t('app', 'Jenis Kelamin'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getAlamat(){
        return $this->hasOne(MAlamat::className(),['id_relasi'=>'id'])->where(['m_alamat.id_tipe' => 44]);
    }

    public function getFile(){
        return $this->hasOne(TFile::className(),['id_relasi'=>'id'])->where(['t_file.id_jenis' => 48]);
    }

    public function getJabatan(){
        return $this->hasOne(TUserJabatan::className(),['id_user'=>'id_user']);
    }

    public function getDeskripsiJabatan() {
        return $this->hasOne(MMaster::className(), ['id' => 'id_role'])->viaTable('t_user_jabatan', ['id_user' => 'id_user']);
    }

    public function getUsaha() {
        return $this->hasOne(MUsaha::className(), ['id' => 'id_usaha'])->viaTable('t_user_jabatan', ['id_user' => 'id_user']);
    }
}
