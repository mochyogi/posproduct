<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_item".
 *
 * @property int $id
 * @property int|null $id_kategori
 * @property string|null $kode_item
 * @property string|null $nama_item
 * @property int|null $status
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 */
class MItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_kategori', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_item', 'nama_item'], 'string', 'max' => 255],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_kategori' => Yii::t('app', 'Id Kategori'),
            'kode_item' => Yii::t('app', 'Kode Item'),
            'nama_item' => Yii::t('app', 'Nama Item'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getItemHarga()
    {
        return $this->hasMany(MItemHarga::className(), ['id_item' => 'id']);
    }

    public function getItemSatuan()
    {
        return $this->hasMany(MItemSatuan::className(), ['id_item' => 'id']);
    }

    public function getFile(){
        return $this->hasOne(TFile::className(),['id_relasi'=>'id'])->where(['t_file.id_jenis' => 48]);
    }
}
