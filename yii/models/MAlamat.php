<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_alamat".
 *
 * @property int $id
 * @property int|null $id_tipe outlet, profile, gudang
 * @property int|null $id_relasi id user atau id outlet atau id gudang
 * @property string|null $alamat
 * @property int|null $id_negara
 * @property int|null $id_provinsi
 * @property int|null $id_kota
 * @property int|null $id_kecamatan
 * @property int|null $kode_pos
 * @property string|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class MAlamat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_alamat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipe', 'id_relasi', 'id_negara', 'id_provinsi', 'id_kota', 'id_kecamatan', 'kode_pos'], 'integer'],
            [['alamat'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_tipe' => Yii::t('app', 'Id Tipe'),
            'id_relasi' => Yii::t('app', 'Id Relasi'),
            'alamat' => Yii::t('app', 'Alamat'),
            'id_negara' => Yii::t('app', 'Id Negara'),
            'id_provinsi' => Yii::t('app', 'Id Provinsi'),
            'id_kota' => Yii::t('app', 'Id Kota'),
            'id_kecamatan' => Yii::t('app', 'Id Kecamatan'),
            'kode_pos' => Yii::t('app', 'Kode Pos'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
