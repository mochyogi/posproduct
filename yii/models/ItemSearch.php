<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MItem;
use app\models\MItemHarga;
use app\models\MItemSatuan;
use app\models\TFile;

/**
 * MMasterSearch represents the model behind the search form of `app\models\MItem`.
 */
class ItemSearch extends MItem
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public $itemHarga;
    public $itemSatuan;
    public function rules()
    {
        return [
            [['id_kategori', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['kode_item', 'nama_item'], 'string', 'max' => 255],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MItem::find();
        $query->innerJoinWith('file', true);
        $query->innerJoinWith('itemHarga', true);
        $query->innerJoinWith('itemSatuan', true);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'id_kategori' => $this->id_kategori,
            'kode_item' => $this->kode_item,
            'nama_item' => $this->nama_item,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'id_kategori', $this->id_kategori])
            ->andFilterWhere(['like', 'kode_item', $this->kode_item])
            ->andFilterWhere(['like', 'nama_item', $this->nama_item])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
}
