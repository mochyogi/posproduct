<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_file".
 *
 * @property int $id
 * @property int|null $id_relasi
 * @property string|null $relasi
 * @property int|null $id_jenis
 * @property string|null $path_file
 * @property string|null $file_full_name
 * @property string|null $file_name
 * @property string|null $file_ext
 * @property int|null $file_size
 * @property string|null $created_at
 * @property string|null $created_by
 */
class TFile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_relasi', 'id_jenis', 'file_size'], 'integer'],
            [['created_at'], 'safe'],
            [['relasi', 'file_ext', 'created_by'], 'string', 'max' => 100],
            [['path_file', 'file_full_name', 'file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_relasi' => Yii::t('app', 'Id Relasi'),
            'relasi' => Yii::t('app', 'Relasi'),
            'id_jenis' => Yii::t('app', 'Id Jenis'),
            'path_file' => Yii::t('app', 'Path File'),
            'file_full_name' => Yii::t('app', 'File Full Name'),
            'file_name' => Yii::t('app', 'File Name'),
            'file_ext' => Yii::t('app', 'File Ext'),
            'file_size' => Yii::t('app', 'File Size'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
        ];
    }
}
