<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_master".
 *
 * @property int $id
 * @property string|null $deskripsi
 * @property string|null $type
 * @property int|null $status
 * @property int|null $parent
 * @property string|null $code
 * @property int|null $order
 * @property string|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class MMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'parent', 'order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['deskripsi', 'created_by'], 'string', 'max' => 255],
            [['type', 'code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'parent' => Yii::t('app', 'Parent'),
            'code' => Yii::t('app', 'Code'),
            'order' => Yii::t('app', 'Order'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getParentDeskripsi()
    {
        return $this->hasOne(MMaster::className(), ['id' => 'parent']);
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        
        if($insert) {
            $lastOrder = (new \yii\db\Query())
                ->select(new \yii\db\Expression('max(`order`) as lastOrder'))
                ->from($this->tableName())
                ->one();

            $newOrder = ($lastOrder['lastOrder'] + 1);

            $this->order = $newOrder;
        }

        return true;
    }
}
