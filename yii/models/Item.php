<?php
namespace app\models\Item;

use app\models\MItem;
use app\models\MItemSatuan;
use app\models\MItemHarga;
use Yii;
use yii\base\Model;
use yii\widgets\ActiveForm;

class ItemForm extends Model
{
    private $_item;
    private $_harga;
    private $_satuan;

    public function rules()
    {
        return [
            [['Item'], 'required'],
            [['Satuan'], 'safe'],
            [['Harga'], 'safe'],
        ];
    }

    public function afterValidate()
    {
        if (!Model::validateMultiple($this->getAllModels())) {
            $this->addError(null); // add an empty error to prevent saving
        }
        parent::afterValidate();
    }

    public function save()
    {
        if (!$this->validate()) {
            return false;
        }
        $transaction = Yii::$app->db->beginTransaction();
        if (!$this->item->save()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveSatuans()) {
            $transaction->rollBack();
            return false;
        }
        if (!$this->saveHargas()) {
            $transaction->rollBack();
            return false;
        }
        $transaction->commit();
        return true;
    }
    
    public function saveSatuans() 
    {
        $keep = [];
        foreach ($this->satuans as $satuan) {
            $satuan->id_item = $this->item->id;
            if (!$satuan->save(false)) {
                return false;
            }
            $keep[] = $satuan->id;
        }
        $query = MItemSatuan::find()->andWhere(['id_item' => $this->item->id]);
        if ($keep) {
            $query->andWhere(['not in', 'id', $keep]);
        }
        foreach ($query->all() as $satuan) {
            $satuan->delete();
        }        
        return true;
    }

    public function getItem()
    {
        return $this->_item;
    }

    public function setItem($item)
    {
        if ($item instanceof Product) {
            $this->_item = $item;
        } else if (is_array($item)) {
            $this->_item->setAttributes($item);
        }
    }

    public function getHargas()
    {
        if ($this->_hargas === null) {
            $this->_hargas = $this->item->isNewRecord ? [] : $this->item->hargas;
        }
        return $this->_hargas;
    }

    private function getHarga($key)
    {
        $harga = $key && strpos($key, 'new') === false ? MItemHarga::findOne($key) : false;
        if (!$harga) {
            $harga = new Parcel();
            $harga->loadDefaultValues();
        }
        return $harga;
    }

    public function getSatuans()
    {
        if ($this->_satuans === null) {
            $this->_satuans = $this->item->isNewRecord ? [] : $this->item->satuans;
        }
        return $this->_satuans;
    }

    private function getSatuan($key)
    {
        $satuan = $key && strpos($key, 'new') === false ? MItemSatuan::findOne($key) : false;
        if (!$satuan) {
            $satuan = new Parcel();
            $satuan->loadDefaultValues();
        }
        return $satuan;
    }

    public function setParcels($parcels)
    {
        unset($parcels['__id__']); // remove the hidden "new Parcel" row
        $this->_parcels = [];
        foreach ($parcels as $key => $parcel) {
            if (is_array($parcel)) {
                $this->_parcels[$key] = $this->getParcel($key);
                $this->_parcels[$key]->setAttributes($parcel);
            } elseif ($parcel instanceof Parcel) {
                $this->_parcels[$parcel->id] = $parcel;
            }
        }
    }

    public function errorSummary($form)
    {
        $errorLists = [];
        foreach ($this->getAllModels() as $id => $model) {
            $errorList = $form->errorSummary($model, [
              'header' => '<p>Please fix the following errors for <b>' . $id . '</b></p>',
            ]);
            $errorList = str_replace('<li></li>', '', $errorList); // remove the empty error
            $errorLists[] = $errorList;
        }
        return implode('', $errorLists);
    }

    private function getAllModels()
    {
        $models = [
            'Item' => $this->item,
        ];
        foreach ($this->hargas as $id => $harga) {
            $models['Harga.' . $id] = $this->hargas[$id];
        }
        foreach ($this->satuans as $id => $satuan) {
            $models['Satuan.' . $id] = $this->satuans[$id];
        }
        return $models;
    }
}