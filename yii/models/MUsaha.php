<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_usaha".
 *
 * @property int $id
 * @property int|null $id_jenis
 * @property string|null $nama_usaha
 * @property string|null $created_by
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class MUsaha extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_usaha';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_jenis'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_usaha', 'created_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_jenis' => Yii::t('app', 'Id Jenis'),
            'nama_usaha' => Yii::t('app', 'Nama Usaha'),
            'created_by' => Yii::t('app', 'Created By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
