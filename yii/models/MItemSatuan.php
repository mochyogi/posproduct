<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "m_item_satuan".
 *
 * @property int $id
 * @property int|null $id_item
 * @property int|null $id_satuan
 * @property int|null $jumlah_satuan
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 */
class MItemSatuan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'm_item_satuan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_item', 'id_satuan', 'jumlah_satuan'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_item' => Yii::t('app', 'Id Item'),
            'id_satuan' => Yii::t('app', 'Id Satuan'),
            'jumlah_satuan' => Yii::t('app', 'Jumlah Satuan'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getItem()
    {
        return $this->hasOne(MItem::className(), ['id' => 'id_item']);
    }
}
