<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_user_jabatan".
 *
 * @property int $id
 * @property int|null $id_user
 * @property int|null $id_role
 * @property int|null $id_usaha
 * @property string|null $created_at
 * @property string|null $created_by
 * @property string|null $updated_at
 */
class TUserJabatan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_user_jabatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_role', 'id_usaha'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'id_role' => Yii::t('app', 'Id Role'),
            'id_usaha' => Yii::t('app', 'Id Usaha'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
