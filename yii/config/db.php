<?php

return [
    // 'class' => 'yii\db\Connection',
    // 'dsn' => 'pgsql:host=127.0.0.1;port=5432;dbname=posproduct',
    // 'username' => 'postgres',
    // 'password' => 'nopassword',
    // 'charset' => 'utf8',

    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=127.0.0.1;dbname=posproduct',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
