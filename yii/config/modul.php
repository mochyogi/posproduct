<?php 
return [
    'admin' => [
        'class' => 'app\modules\admin\Module',
    ],
    'item' => [
        'class' => 'app\modules\item\Module',
    ],
    'pegawai' => [
        'class' => 'app\modules\pegawai\Module',
    ],
    'stok' => [
        'class' => 'app\modules\stok\Module',
    ],
    'akuntansi' => [
        'class' => 'app\modules\akuntansi\Module',
    ],
    'rbac' => [
    	'class' => 'mdm\admin\Module',
        'layout' => 'left-menu',
        'mainLayout' => '@app/views/layouts/main.php',
    	'controllerMap' => [
    		'class' => 'mdm\admin\controllers\AssignmentController',
    		'userClassName' => 'app\models\User',
    		'idField' => 'id'
    	],
    ],
];