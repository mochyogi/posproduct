<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

$this->title = 'Password Recovery';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header justify-content-center"><h3 class="font-weight-light my-4"><?= Html::encode($this->title) ?></h3></div>
                <div class="card-body">
                	<div class="small mb-3 text-muted">Enter your email address and we will send you a link to reset your password.</div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'request-password-reset-form',
                    ]); ?>
                        <div class="form-group">
                            <?= $form->field($model, 'email')->textInput(['autofocus' => true,'class'=>'form-control py-4','id'=>'inputEmailAddress'])->label('Email',['class'=>'small mb-1']) ?>
                        </div>
                        <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                            <a class="small" href="<?=Url::toRoute('/site/login')?>">Return to login</a>
                            <?= Html::submitButton('Reset Password', ['class' => 'btn btn-primary']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="card-footer text-center">
                    <div class="small"><a href="<?=Url::toRoute('/site/signup')?>">Need an account? Sign up!</a></div>
                </div>
            </div>
        </div>
    </div>
</div>