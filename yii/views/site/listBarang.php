<?php 

use yii\widgets\ListView;
use yii\helpers\Html;

?>
<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
	<div class="card">
	    <div class="row no-gutters">
	        <div class="col-md-4"><img class="img-fluid" src="<?= CustomHelper::getFile($model['file']['id'])?>" alt="..."></div>
	        <div class="col-md-8">
	            <div class="card-body">
	                <h5 class="card-title">Card Image (Left)</h5>
	                <p class="card-text">...</p>
	            </div>
	        </div>
	    </div>
	</div>
</div>