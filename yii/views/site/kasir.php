<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
?>
<div class="container-fluid">
    <div class="row" style="margin-top: 10px; margin-bottom: 10px">
        <div class="col-md-10">
            <?php $form = ActiveForm::begin(['options' => [
                    'style' => 'width:100%'
                 ]
             ]); ?>
                <?= Html::input('text','pencarian','', $options=['class'=>'form-control', 'id'=>'pencarianBarang']) ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-2">
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#form-add-nota-barang" aria-expanded="false" aria-controls="form-add-nota-barang" style="width:100%">Tambah Nota</button>
        </div>
    </div>
    <div class="row" id="daftarBarangContent">
        <div class="col-md-12">
        <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'options' => [
                    'tag' => 'div',
                    'class' => 'row',
                    // 'id' => 'list-wrapper',
                ],
                'itemView' => function ($model, $key, $index, $widget) {
                    return $this->render('listBarang', ['model' => $model]);
                },
                'itemOptions' => [
                    'tag' => false
                ],
                'summary' => '',
                'layout' => '{items} {pager}',

                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last',
                    'maxButtonCount' => 4,
                    'options' => [
                        'class' => 'pagination col-md-12'
                    ]
                ],
            ]);
        ?>
        </div>
        <div class="collapse" id="form-add-nota-barang">
        tes
        </div>
    </div>
</div>
<script type="text/javascript">
    var start = new Date();
    // set end date to max one year period:
    var end = new Date(new Date().setYear(start.getFullYear()+1));

    $('#fromDate').datepicker({
        startDate : start,
        endDate   : end
    // update "toDate" defaults whenever "fromDate" changes
    }).on('changeDate', function(){
        // set the "toDate" start to not be later than "fromDate" ends:
        $('#toDate').datepicker('setStartDate', new Date($(this).val()));
    }); 

    $('#toDate').datepicker({
        startDate : start,
        endDate   : end
    // update "fromDate" defaults whenever "toDate" changes
    }).on('changeDate', function(){
        // set the "fromDate" end to not be later than "toDate" starts:
        $('#fromDate').datepicker('setEndDate', new Date($(this).val()));
    });
</script>