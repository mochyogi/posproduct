<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card shadow-lg border-0 rounded-lg mt-5">
                <div class="card-header justify-content-center"><h3 class="font-weight-light my-4">Login</h3></div>
                <div class="card-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                    ]); ?>
                        <div class="form-group">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control py-4','id'=>'inputEmailAddress'])->label('Username',['class'=>'small mb-1']) ?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control py-4','id'=>'inputPassword','placeholder'=>'Enter password'])->label('Password',['class'=>'small mb-1']) ?>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <?= $form->field($model, 'rememberMe')->checkbox(['class'=>'custom-control-input','id'=>'rememberPasswordCheck'])->label('Remember password',['class'=>'custom-control-label']) ?>
                            </div>
                        </div>
                        <div class="form-group d-flex align-items-center justify-content-between mt-4 mb-0">
                            <a class="small" href="<?=Url::toRoute('/site/request-password-reset')?>">Forgot Password?</a>
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="card-footer text-center">
                    <div class="small"><a href="<?=Url::toRoute('/site/signup')?>">Need an account? Sign up!</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
