<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = $exception->getCode();
?>
<div id="layoutError">
    <div id="layoutError_content">
        <main>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="text-center mt-4">
                            <h1 class="display-1"><?=$exception->getCode()?></h1>
                            <p class="lead"><?=$exception->getMessage()?></p>
                            <a href="<?= Url::home()?>"><i class="fas fa-arrow-left mr-1"></i>Return to Dashboard</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
    <div id="layoutError_footer">
        <footer class="sb-footer py-4 mt-auto sb-footer-light">
            <div class="container-fluid">
                <div class="d-flex align-items-center justify-content-between small">
                    <div>Copyright &copy; Your Website 2019</div>
                    <div>
                        <a href="#">Privacy Policy</a>
                        &middot;
                        <a href="#">Terms &amp; Conditions</a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>