<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \mdm\admin\models\form\Signup */

$this->title = 'Create an Account';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row justify-content-center">
        <!-- Register Form-->
        <div class="col-xl-8 col-lg-9">
            <div class="card my-5">
                <div class="card-body p-5 text-center">
                    <div class="h3 font-weight-light mb-3">Create an Account</div>
                    <div class="small text-muted mb-2">Sign in using...</div>
                    <a class="btn sb-btn-icon sb-btn-facebook mx-1" href="javascript:void(0);"><i class="fab fa-facebook-f fa-fw fa-sm"></i></a><a class="btn sb-btn-icon sb-btn-github mx-1" href="javascript:void(0);"><i class="fab fa-github fa-fw fa-sm"></i></a><a class="btn sb-btn-icon sb-btn-google mx-1" href="javascript:void(0);"><i class="fab fa-google fa-fw fa-sm"></i></a><a class="btn sb-btn-icon sb-btn-twitter mx-1" href="javascript:void(0);"><i class="fab fa-twitter fa-fw fa-sm"></i></a>
                </div>
                <hr class="my-0" />
                <div class="card-body p-5">
                    <div class="text-center small text-muted mb-4">...or enter your information below.</div>
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                    	<div class="form-group">
                        	<?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control sb-form-control-solid py-4','id'=>'Username'])->label('Username',['class'=>'text-gray-600 small']) ?>
                        </div>
                        <div class="form-group">
                        	<?= $form->field($model, 'email')->textInput(['class'=>'form-control sb-form-control-solid py-4','id'=>'email'])->label('Email Address',['class'=>'text-gray-600 small']) ?>
                        </div>
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                	<?= $form->field($model, 'password')->passwordInput(['class'=>'form-control sb-form-control-solid py-4','id'=>'inputPassword'])->label('Password',['class'=>'text-gray-600 small']) ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                	<?= $form->field($model, 'retypePassword')->passwordInput(['class'=>'form-control sb-form-control-solid py-4','id'=>'reinputPassword'])->label('Confirm Password',['class'=>'text-gray-600 small']) ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group d-flex align-items-center justify-content-between">
                            <?= Html::submitButton('Create Account', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <hr class="my-0" />
                <div class="card-body px-5 py-4">
                    <div class="small text-center">Have an account? <?= Html::a('Sign In!', ['site/login']) ?></div>
                </div>
            </div>
        </div>
    </div>
</div>