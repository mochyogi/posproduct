<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use mdm\admin\components\MenuHelper;
use mdm\admin\components\Helper as Help;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;
use app\components\CustomHelper;
use xtetis\bootstrap4glyphicons\assets\GlyphiconAsset;
AppAsset::register($this);
GlyphiconAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" type="image/x-icon" href="<?= Url::home();?>img/app/favicon.png" />
    <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.24.1/feather.min.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&amp;display=swap" rel="stylesheet" />
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
    <body class="sb-nav-fixed">
        <?php $this->beginBody() ?>
        <?php 
        $callback = function($menu){
            $data = eval($menu['data']);
            return [
                'label' => $menu['name'], 
                'url' => [$menu['route']],
                'options' => $data,
                'items' => $menu['children']
            ];
        };
        $anakna = MenuHelper::getAssignedMenu(Yii::$app->user->id, null, $callback,true);
        NavBar::begin([
            'brandUrl'      => Url::home(true),
            'brandImage'    => Url::home().'img/app/sbpro-logo.svg',
            'brandOptions'  => ['class'=>'navbar-brand d-none d-sm-block'],
            'renderInnerContainer' => false,
            'collapseOptions' => ['class'=>'collapse navbar-collapse','id'=>'navbarSupportedContent'],
            'options'       => ['id'=>'sidenavAccordion','class'=>'sb-topnav navbar navbar-expand shadow navbar-light bg-white'
            ],

        ]);

            echo Nav::widget([
                'items' => $anakna,
                'options' =>['class'=>'navbar-nav ml-auto'],
            ]);?>
            <ul class="navbar-nav align-items-center">
                <li class="nav-item dropdown no-caret mr-3 sb-dropdown-notifications">
                    <a class="btn sb-btn-icon sb-btn-transparent-dark dropdown-toggle" id="navbarDropdownAlerts" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
                    <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownAlerts">
                        <h6 class="dropdown-header sb-dropdown-notifications-header"><i class="mr-2" data-feather="bell"></i>Alerts Center</h6>
                        <a class="dropdown-item sb-dropdown-notifications-item" href="javascript:void(0);"
                            ><div class="sb-dropdown-notifications-item-icon bg-warning"><i data-feather="activity"></i></div>
                            <div class="sb-dropdown-notifications-item-content">
                                <div class="sb-dropdown-notifications-item-content-details">December 29, 2019</div>
                                <div class="sb-dropdown-notifications-item-content-text">This is an alert message. It's nothing serious, but it requires your attention.</div>
                            </div></a
                        ><a class="dropdown-item sb-dropdown-notifications-item" href="javascript:void(0);"
                            ><div class="sb-dropdown-notifications-item-icon bg-info"><i data-feather="bar-chart"></i></div>
                            <div class="sb-dropdown-notifications-item-content">
                                <div class="sb-dropdown-notifications-item-content-details">December 22, 2019</div>
                                <div class="sb-dropdown-notifications-item-content-text">A new monthly report is ready. Click here to view!</div>
                            </div></a
                        ><a class="dropdown-item sb-dropdown-notifications-item" href="javascript:void(0);"
                            ><div class="sb-dropdown-notifications-item-icon bg-danger"><i class="fas fa-exclamation-triangle"></i></div>
                            <div class="sb-dropdown-notifications-item-content">
                                <div class="sb-dropdown-notifications-item-content-details">December 8, 2019</div>
                                <div class="sb-dropdown-notifications-item-content-text">Critical system failure, systems shutting down.</div>
                            </div></a
                        ><a class="dropdown-item sb-dropdown-notifications-item" href="javascript:void(0);"
                            ><div class="sb-dropdown-notifications-item-icon bg-success"><i data-feather="user-plus"></i></div>
                            <div class="sb-dropdown-notifications-item-content">
                                <div class="sb-dropdown-notifications-item-content-details">December 2, 2019</div>
                                <div class="sb-dropdown-notifications-item-content-text">New user request. Woody has requested access to the organization.</div>
                            </div></a
                        ><a class="dropdown-item sb-dropdown-notifications-footer" href="javascript:void(0);">View All Alerts</a>
                    </div>
                </li>
                <li class="nav-item dropdown no-caret mr-3 sb-dropdown-user">
                    <a class="btn sb-btn-icon sb-btn-transparent-dark dropdown-toggle" id="navbarDropdownUserImage" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="padding: 0 !important;"><img class="img-fluid" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"/></a>
                    <div class="dropdown-menu dropdown-menu-right border-0 shadow animated--fade-in-up" aria-labelledby="navbarDropdownUserImage">
                        <h6 class="dropdown-header d-flex align-items-center">
                            <img class="sb-dropdown-user-img" src="https://source.unsplash.com/QAB-WJcbgJk/60x60" />
                            <div class="sb-dropdown-user-details">
                                <div class="sb-dropdown-user-details-name">Valerie Luna</div>
                                <div class="sb-dropdown-user-details-email">vluna@aol.com</div>
                            </div>
                        </h6>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0);"
                            ><div class="sb-dropdown-item-icon"><i data-feather="settings"></i></div>
                            Account</a
                        ><a class="dropdown-item" href="<?=Url::toRoute('/site/logout')?>"
                            ><div class="sb-dropdown-item-icon"><i data-feather="log-out"></i></div>
                            Logout</a
                        >
                    </div>
                </li>
            </ul>
        <?php NavBar::end();?>
        
        <div id="layoutSidenav">
            <div id="layoutSidenav_content">
                <main>
                <?= $content ?>
                </main>
                <footer class="sb-footer py-4 mt-auto sb-footer-light">
                    <div class="container-fluid">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div>Copyright &copy; Your Website 2019</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
