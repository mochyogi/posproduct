<?php

namespace app\components;

use Yii;
use yii\base\Component;
use Swift_SmtpTransport;
use Swift_Message;
use Swift_Mailer;
use yii\helpers\Url;
use yii\db\Expression;
use yii\db\Query;
use app\models\MMaster;
use app\models\TFile;

class CustomHelper {

    public static function getMenu(){
        $master = MMaster::find()->where(['parent' => 0])->all();
        $listMenu = [];
        foreach ($master as $key => $value) {
            array_push($listMenu, $value['deskripsi']);
        }
        return $listMenu;
    }

    public static function getDetailMenu($parent){
        $master = MMaster::find()->where(['parent' => $parent])->all();
        // $listMenu = [];
        // foreach ($master as $key => $value) {
        //     array_push($listMenu, $value['deskripsi']);
        // }

        // return $listMenu;
        return $master;
    }

    public function prosesReadUpload($filedata = null){
        $dataarr = array();
        foreach ($filedata as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if (is_array($v2)) {
                        foreach ($v2 as $k3 => $v3) {
                            $dataarr[$k2][$k][] = $v3;
                        }
                    } else {
                        $dataarr[$k][] = $v2;
                    }
                }
            } else {
                $dataarr[$k] = $v;
            }
        }
        return $dataarr;
    }

    public function prosesUpload($filedata, $type)
    {
        $dataupload = $this->prosesReadUpload($filedata);
        $name = $dataupload['file_name'];
        $file_tmp = $dataupload['tmp_name'];
        $file_size = $dataupload['size'];
        $fileexplode = explode(".", $name);
        $filename = $fileexplode[0];
        $extgab = array();
        if (count($fileexplode) >= 2) {
            for ($i = 0; $i < count($fileexplode); $i++) {
                if ($i != 0) {
                    $extgab[] = $fileexplode[$i];
                }
            }
        }
        $fileext = implode(".", $extgab);
        $fullname = $nik 
            . '_' . uniqid()
            . '_' . (round(microtime(true) * 1000))
            . '_' . $filename
            . "." 
            . $fileext;
        // $fullpathfile = Yii::getAlias('@webroot/fileupload/'. $fullname);
        $ass = move_uploaded_file($file_tmp, $fullpathfile);
        $pathfile = ('/' . $dirfoto . '/' . $fullname);

        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'endpoint' => \Yii::$app->params['s3']['endpoint'],
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key'    => \Yii::$app->params['s3']['key'],
                'secret' => \Yii::$app->params['s3']['secret'],
            ],
        ]);
        
        try {
            $insert = $s3->putObject([
                'Bucket' => \Yii::$app->params['s3']['bucket'],
                'Key'    => $fullname,
                'SourceFile' => $file_tmp,
                'ACL'    => 'public-read'
            ]);
        } catch (S3Exception $e) {
            echo $e->getMessage() . "\n";
            exit;
        }

        $data = $this->{$functionexist}($jenis, $nik, $fullname, $fullname, $name, $fileext, $file_size);

        return $data;
    }

    public static function GetFile($id_file){
        $model = TFile::find()->where(['id' => $id_file])->one();
        if (!empty($model)):
            $url= Url::home().$model->path_file;
        else:
            $url = Url::home().'img/app/default-user.png';
        endif;

        return $url;
    }

    public static function GetFiles($id_file){
        $url="";
        $foto = \app\models\Filesave::find()->where('id=:id',[':id'=>$id_file])->One();
        if (!empty($foto)):
            // $url=Url::to(['/site/file', 'file_id' => $foto->id]);
            $url = 'https://rekrutbersama.fhcibumn.com/site/file/file_id='.$foto->id;
        else:
            $url = "http://www.placehold.it/200x150/EFEFEF/AAAAAA?text=no+image";
        // get main server
        // $mainServer = \app\models\MMaster::find()
        //     ->where(['type' => 'URL_MAIN_SERVER', 'status' => '1'])
        //     ->one()
        //     ->deskripsi;
        // $url=$mainServer . "accept/get-file?id=" . $id_file;
        endif;
        // return 'tes';
        return $url;
    }

    public static function dropdownMaster($type,$id = null){
        if($type == "agama"){
            $type = 'agama';
        } else if($type == "provinsi"){
            $type = 'provinsi';
        } else if($type == "jenjang"){
            $type = 'jenjang';
        } else if($type == "kotainstitusi"){
            $type = 'kotainstitusi';
        } else if($type == "kategori_pres"){
            $type = 'kategori_prestasi';
        } else if($type == "pencapaian"){
            $type = 'pencapaian_prestasi';
        } else if($type == "kepesertaan"){
            $type = 'kepesertaan_prestasi';
        } else if($type == "level_prestasi"){
            $type = 'level_prestasi';
        } else if($type == "kabkota"){
            $type = 'kabkota';
        } else if ($type == "masterjurusan"){
            $type = 'masterjurusan';
        } else if ($type == "subkategoriprestasi"){
            $type = 'subkategoriprestasi';
        } else if ($type == "file_pendukung"){
            $type = 'file_pendukung';
        }

        $data_tes = MMaster::find()->where(['type' => $type, 'status' => 1])->orderBy(['order' =>SORT_ASC])->one();
        if($data_tes->order == null || empty($data_tes->order)){
            $data = MMaster::find()->where(['type' => $type, 'status' => 1])->orderBy(['deskripsi' =>SORT_ASC])->all();
        }else{
            $data = MMaster::find()->where(['type' => $type, 'status' => 1])->orderBy(['order' =>SORT_ASC])->all();
        }
        foreach ($data as $key => $row) {
            if (!empty($id) && $id != ''){
                if ($id == $row['id_master']){
                    echo '<option title="'.$row['deskripsi'].'" value='.$id.' selected>'.$row['deskripsi'].'</option>';
                } else {
                    echo '<option title="'.$row['deskripsi'].'" value='.$row['id_master'].'>'.$row['deskripsi'].'</option>';
                }
            } else {
                echo '<option title="'.$row['deskripsi'].'" value='.$row['id_master'].'>'.$row['deskripsi'].'</option>';
            }
        }
    }

    public static function send_email($param) 
    {
        $mailer = Yii::$app->mail; 
        try {
            
            $recipient = $param['email'];
            $subject = $param['subject'];
            $body = $param['body'];
            $mailer->compose()
                ->setFrom([Yii::$app->params['supportEmail'] => 'Admin FHCI'])
                ->setTo($recipient)
                ->setSubject($subject)
                ->setHtmlBody($body)
                ->setCharset('UTF-8')
                ->send();
       
        } catch (\Swift_TransportException $e) {

            $mailer->getTransport()->stop();
            sleep(10); // Just in case ;-)
        }
    }

    public static function dateIndo($date){
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

        $tahun = substr($date, 0, 4);
        $bulan = substr($date, 5, 2);
        $tgl   = substr($date, 8, 2);

        $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;    
        return($result);
    }

    public static function bulanIndo($date){
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $bulan = substr($date, 5, 2);
        $result = $BulanIndo[(int)$bulan-1];    
        return($result);
    }

    public function getSimpleMaster($type)
    {
        $data = (new \Yii\db\Query())
                ->select('*')
                ->from('m_master')
                ->where('type = :type', [':type' => $type])
                ->all();

        return $data;
    }

    public function getMaster($id)
    {
        $data = (new \Yii\db\Query())
                ->select('*')
                ->from('m_master')
                ->where('id_master = :id', [':id' => $id])
                ->one();

        return $data;
    }
    
    public static function getOneMaster($id_master)
    {
        $data = (new \Yii\db\Query())
                ->select('*')
                ->from('m_master')
                ->where('id_master = :idmaster', [':idmaster' => $id_master])
                ->one();
        return $data['deskripsi'];
    }

    public static function getParent($id_master)
    {
        $parent = MMaster::find()->where(['id'=> $id_master])->one();
        if(isset($parent)){
            $deskripsi = $parent->deskripsi;
        }else{
            $deskripsi = '';
        }
        return $deskripsi;
    }

    public function safe_b64encode($string) {
    
        $data = base64_encode($string);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return $data;
    }
 
    public function safe_b64decode($string) {
        $data = str_replace(array('-','_'),array('+','/'),$string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public static function encode($value){ 
        $skey   = "SuPerEncKey2010x";
        if(!$value){return false;}
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $skey, $text, MCRYPT_MODE_ECB, $iv);
        $data = base64_encode($crypttext);
        $data = str_replace(array('+','/','='),array('-','_',''),$data);
        return trim($data); 
    }
    
    public static function decode($value){
        $skey   = "SuPerEncKey2010x";
        if(!$value){return false;}
        $crypttext =  str_replace(array('-','_'),array('+','/'),$value);
        $mod4 = strlen($crypttext) % 4;
        if ($mod4) {
            $crypttext .= substr('====', $mod4);
        }
        $crypttext =base64_decode($crypttext);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public static function longDateTime($datetime){
        $explode = explode(" ", $datetime);
        $tgl    = explode("-", $explode[0]);
        $jam = explode(":", $explode[1]);

        $bulan  = $tgl[1];
        if($bulan == "01"){
            $bulan  = "Januari";
        }elseif($bulan == "02"){
            $bulan  = "Februari";
        }elseif($bulan == "03"){
            $bulan  = "Maret";
        }elseif($bulan == "04"){
            $bulan  = "April";
        }elseif($bulan == "05"){
            $bulan  = "Mei";
        }elseif($bulan == "06"){
            $bulan  = "Juni";
        }elseif($bulan == "07"){
            $bulan  = "Juli";
        }elseif($bulan == "08"){
            $bulan  = "Agustus";
        }elseif($bulan == "09"){
            $bulan  = "September";
        }elseif($bulan == "10"){
            $bulan  = "Oktober";
        }elseif($bulan == "11"){
            $bulan  = "November";
        }else{
            $bulan  = "Desember";
        }
        $longDateTime = array(
            'tgl'=> intval($tgl[2]),
            'bulan' => $bulan,
            'tahun' => $tgl[0],
            'jam' => $jam[0],
            'menit' => $jam[1],
            'detik' => $jam[2],
            'all' => $explode[1],
        );
        return $longDateTime['tgl'].' '.$longDateTime['bulan'].' '.$longDateTime['tahun'].' pukul '.$longDateTime['jam'].':'.$longDateTime['menit'].' WIB';
        // return $longDateTime['all'];
    }
}