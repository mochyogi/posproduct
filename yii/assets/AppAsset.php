<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/styles.css',
        'https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css',
        'css/select2.min.css',
        'css/loading.css',
        'https://cdn.jsdelivr.net/npm/sweetalert2@8.19.0/dist/sweetalert2.min.css',
    ];
    public $js = [
        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js',
        'js/scripts.js',
        'js/select2.min.js',
        // 'https://code.jquery.com/jquery-3.4.1.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js',
        'https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js',
        'https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js',
        // 'js/chart-area-demo.js',
        'js/bootstrap-datepicker.min.js',
        // 'js/chart-bar-demo.js',
        // 'js/datatables-demo.js',
        // 'js/chart-pie-demo.js',
        //'js/datatables-demo.js',
        'https://cdn.jsdelivr.net/npm/sweetalert2@8.19.0/dist/sweetalert2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        // 'yii\bootstrap4\BootstrapAsset',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
}
